# top-pricing

Implements the Mint Software Engineer
[Challenge](https://github.com/go-mint/code-challenges/blob/master/software-engineer-challenge.md#mint-software-engineer-challenge)

## Architecture

The implementation consists of three microservices written in Go and
communicating internally via gRPC:

* api
* pricing
* ranking

The frontend API implements the REST API documented in the requirements and also
gRPC clients for the ranking and pricing microservices.

### Logic

* when receiving a request on the REST API microservice, first determine the
  ranking(with or without a limit parameter), in order to get the list of coins
  that will be returned to the user.
* using the list of coins returned from the ranking, request their prices to the
  pricing microservice
* merge the data from both data sources and return it to the REST API client

In order to always have fresh data, both the ranking and pricing microservices
perform upstream requests for data whenever they are called and don't cache any
data locally. This could be an optimization, but would violate the requirement
for data freshness in all conditions.

### Rationale for using gRPC

* slightly more efficient and faster than plain REST over HTTP
* explicit marshaling/unmarshaling of data is not needed
* simpler/minimalist architecture and no single point of failure, bottleneck or
  additional latency, which can happen when using a queue or database.

### Libraries

For both data sources I was able to find relatively good native Go libraries
that abstract the official REST APIs. These were convenient to use, although in
some case they seem to introduce additional unneeded processing: the one used
for cryptocompare sorts all the data alphabetically, which is not needed for our
use case.

### Known issues/possible room for improvement

* Test coverage is abysmal, currently only covering a couple of the most
  critical parts, but till helped a lot when refactoring.
* Poor error handling when the backends are down, sometimes panics can be seen
  in the logs.
* Very few comments throughout the code
* Make use of Context for tracing request IDs in the logs
* Cache some of the data that's unlikely to change?

## Getting started

* Make sure you have `Docker` and `docker-compose` already installed.
* Execute `docker-compose up` in a terminal to build and start all the services.
  Note, the initial build may take a while.
* In another terminal, you can run `curl http://localhost:8080?limit=10`. You
  can also use a normal browser against the same
  [URL](http://localhost:8080?limit=10)
* If you change the limit query string parameter you can get more entries. When
  the `limit` parameter is missing, the API will return all the coin data,
  currently consisting of about 2600 coins.