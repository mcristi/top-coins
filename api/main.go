package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/mcristi/top-coins/pricing/pricing_pb"
	"google.golang.org/grpc"

	ranking "bitbucket.org/mcristi/top-coins/ranking/ranking_pb"
)

// API data structure
type API struct {
	listenAddress string
	pricingHost   string
	rankingHost   string
	pricingConn   *grpc.ClientConn
	rankingConn   *grpc.ClientConn
	pricingClient pricing.PricingClient
	rankingClient ranking.RankingClient
}

func (a *API) connectToBackendServices() {
	var err error
	log.Println("Connecting to the pricing service on", a.pricingHost)
	a.pricingConn, err = grpc.Dial(a.pricingHost, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect to pricing backend: %v", err)
	}

	log.Println("Connecting to the ranking service on", a.rankingHost)
	a.rankingConn, err = grpc.Dial(a.rankingHost, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect to ranking backend: %v", err)
	}

	a.pricingClient = pricing.NewPricingClient(a.pricingConn)
	a.rankingClient = ranking.NewRankingClient(a.rankingConn)

}

func (a *API) disconnectBackends() {
	a.pricingConn.Close()
	a.rankingConn.Close()
}

func (a *API) parseCommandLineFlags() {
	flag.StringVar(&a.pricingHost, "pricing-host", "", "The address:port of the pricing service")
	flag.StringVar(&a.rankingHost, "ranking-host", "", "The address:port of the ranking service")
	flag.StringVar(&a.listenAddress, "listen-on", ":8080", "The address:port where to bind the API service")

	flag.Parse()
	if a.pricingHost == "" || a.rankingHost == "" {
		log.Fatalln("Missing command-line arguments, run with -help for more information")
	}
}

func (a *API) listenAndServe() {

	log.Println("Starting API HTTP server on", a.listenAddress)
	http.HandleFunc("/", a.httpHandler)
	log.Fatal(http.ListenAndServe(a.listenAddress, nil))
}

func (a *API) httpHandler(w http.ResponseWriter, r *http.Request) {

	limit, _ := strconv.ParseInt(r.FormValue("limit"), 10, 64)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	top, err := a.rankingClient.Top(ctx, &ranking.RankingRequest{Limit: limit})
	if err != nil || top == nil || len(top.Top) == 0 {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("503 - Service Unavailable"))
		log.Printf("could not get ranking data, failing: %v", err)
	}

	coinSymbols := getCoinList(top.Top)

	prices, err := a.pricingClient.Prices(
		ctx,
		&pricing.PricingRequest{CoinNames: coinSymbols},
	)
	if err != nil {
		log.Fatalf("could not get top: %v", err)
	}

	writeResponse(w, top.Top, prices.PriceData)
}
func getCoinList(top []*ranking.Coin) []string {
	var list []string
	for _, item := range top {
		if item != nil {
			list = append(list, item.CoinName)
		}
	}
	log.Println(list)
	return list
}

func writeResponse(w http.ResponseWriter,
	top []*ranking.Coin,
	prices map[string]float64,
) {
	fmt.Fprintln(w, "Rank,\tSymbol,\tPrice USD")
	for _, coin := range top {
		fmt.Fprintf(w, "%d,\t%s,\t%f\n", coin.Rank, coin.CoinName, prices[coin.CoinName])
	}
}
func main() {
	api := API{}

	api.parseCommandLineFlags()

	api.connectToBackendServices()
	defer api.disconnectBackends()

	api.listenAndServe()

}
