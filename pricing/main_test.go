package main

import (
	"reflect"
	"testing"

	pricing "bitbucket.org/mcristi/top-coins/pricing/pricing_pb"
)

func TestPricingServer_FilterCoins(t *testing.T) {

	tests := []struct {
		name      string
		s         *PricingServer
		coins     CoinMap
		coinNames []string
		want      *pricing.PricingResponse
	}{
		{
			name: "test coin data filtering",
			coins: CoinMap{
				"1": {Symbol: "BTC", PriceUSD: 8000},
				"3": {Symbol: "ETH", PriceUSD: 500},
				"5": {Symbol: "XRP", PriceUSD: 0.5},
			},
			coinNames: []string{"BTC", "XRP"},
			want: &pricing.PricingResponse{
				PriceData: map[string]float64{
					"BTC": 8000,
					"XRP": 0.5,
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &PricingServer{}
			if got := s.FilterCoins(tt.coins, tt.coinNames); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PricingServer.FilterCoins() = %v, want %v", got, tt.want)
			}
		})
	}
}
