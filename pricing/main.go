//go:generate protoc -I ./pricing_pb --go_out=plugins=grpc:./pricing_pb ./pricing_pb/pricing.proto
package main

import (
	"context"
	"fmt"
	"log"
	"net"

	pricing "bitbucket.org/mcristi/top-coins/pricing/pricing_pb"
	cmc "github.com/miguelmota/go-coinmarketcap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":5000"
)

// PricingServer implements all the logic
type PricingServer struct{}

// CoinMap is a map of coins
type CoinMap map[string]cmc.Coin

// FetchPrices requests the latest price data from the coinmarketcap API
func (s *PricingServer) FetchPrices() (CoinMap, error) {

	coins, err := cmc.GetAllCoinData(0)
	if err != nil {
		return CoinMap{}, err
	}
	return coins, nil
}

// FilterCoins filters the CoinMap data structure, returning only the data for
// the coins given in the coinNames parameter as a slice, converted into a
// smaller data structure that only contains the coin name and the price
func (s *PricingServer) FilterCoins(coins CoinMap, coinNames []string) *pricing.PricingResponse {

	var filteredData pricing.PricingResponse
	price := make(map[string]float64)

	for _, coin := range coins {
		for _, name := range coinNames {

			if coin.Symbol != name {
				continue
			}

			price[coin.Symbol] = coin.PriceUSD
			filteredData.PriceData = price

		}
	}

	return &filteredData
}

// Prices implements the Prices rpc call
func (s *PricingServer) Prices(ctx context.Context,
	in *pricing.PricingRequest) (*pricing.PricingResponse, error) {

	prices, err := s.FetchPrices()
	if err != nil {
		return nil, err
	}

	return s.FilterCoins(prices, in.CoinNames), nil
}

func main() {
	fmt.Println("Starting pricing server")

	// gRPC boilerplate
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pricing.RegisterPricingServer(s, &PricingServer{})

	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
