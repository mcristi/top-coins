//go:generate protoc -I ./ranking_pb --go_out=plugins=grpc:./ranking_pb ./ranking_pb/ranking.proto
package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"sort"
	"strconv"

	ccg "github.com/lucazulian/cryptocomparego"

	ranking "bitbucket.org/mcristi/top-coins/ranking/ranking_pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":5001"
)

// RankingServer implements all the logic
type RankingServer struct{}

// CoinTop is a slice of coins in the original form
type CoinTop []ccg.Coin

// CoinSorter allows us to sort the simplified coin data structure by rank
type CoinSorter []*ranking.Coin

func (a CoinSorter) Len() int           { return len(a) }
func (a CoinSorter) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a CoinSorter) Less(i, j int) bool { return a[i].Rank < a[j].Rank }

// FetchRanking requests all the ranking data from the cryptocompare API
func (s *RankingServer) FetchRanking() (CoinTop, error) {

	ctx := context.TODO()

	client := ccg.NewClient(nil)
	coins, _, err := client.Coin.List(ctx)
	if err != nil {
		fmt.Printf("Something bad happened: %s\n", err)
		return CoinTop{}, err
	}

	return coins, nil
}

// GetTop filters the CoinTop data structure, returning only the top<limit>
// coins, converted into a smaller data structure that only contains the coin
// name and its order
func (s *RankingServer) GetTop(coins CoinTop, limit int64) []*ranking.Coin {

	var filteredData []*ranking.Coin

	for _, coin := range coins {
		order, _ := strconv.ParseInt(coin.SortOrder, 10, 64)

		if limit > 0 && order > limit {
			continue
		}

		filteredData = append(
			filteredData,
			&ranking.Coin{
				CoinName: coin.Name,
				Rank:     order,
			},
		)
	}

	sort.Sort(CoinSorter(filteredData))
	return filteredData
}

// Top implements the Top rpc call
func (s *RankingServer) Top(ctx context.Context,
	in *ranking.RankingRequest) (*ranking.RankingResponse, error) {

	all, err := s.FetchRanking()
	if err != nil {
		return nil, err
	}

	return &ranking.RankingResponse{
		Top: s.GetTop(all, in.Limit)}, nil
}

func main() {
	fmt.Println("Starting ranking server")

	// gRPC boilerplate
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	ranking.RegisterRankingServer(s, &RankingServer{})

	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
