package main

import (
	"reflect"
	"testing"

	ranking "bitbucket.org/mcristi/top-coins/ranking/ranking_pb"
)

func TestRankingServer_GetTop(t *testing.T) {

	tests := []struct {
		name  string
		s     *RankingServer
		coins CoinTop
		limit int64
		want  []*ranking.Coin
	}{
		{
			name: "test coin data filtering",
			coins: CoinTop{
				{Name: "BTC", SortOrder: "1"},
				{Name: "ETH", SortOrder: "2"},
				{Name: "XRP", SortOrder: "3"},
			},
			limit: 2,
			want: []*ranking.Coin{
				&ranking.Coin{CoinName: "BTC", Rank: 1},
				&ranking.Coin{CoinName: "ETH", Rank: 2},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &RankingServer{}
			if got := s.GetTop(tt.coins, tt.limit); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RankingServer.GetTop() = %v, want %v", got, tt.want)
			}
		})
	}
}
